(function($) {
  'use strict';
  $(function() {
    $('#order-listing-in').DataTable({
        "lengthChange": true,
        "aLengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
      ],

      "columnDefs": [
    { "width": "20%", "targets": 1 }
  ],
        dom: 'Bfrtip',
        buttons: [
        {
            extend: 'excel',
            text: 'Export Current Page',
            exportOptions: {
                modifier: {
                    page: 'current'
                }, columns: 'th:not(:last-child)'
            }
        }, 
        {
          extend: 'excel',
          text: 'Export All Data',
          exportOptions: {
            columns: 'th:not(:last-child)'
          }
        },
        'pageLength',
        'colvis'
    ],
      select: true,
      "iDisplayLength": 10,
        
      "language": {
        search: ""
      }
    });
    $('#order-listing-in').each(function() {
      var datatable = $(this);
      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
      search_input.attr('placeholder', 'Search');
      search_input.removeClass('form-control-sm');
      // LENGTH - Inline-Form control
      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
      length_sel.removeClass('form-control-sm');
    });

    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
            var startDate = new Date(data[5]);
            if (min == null && max == null) { return true; }
            if (min == null && startDate <= max) { return true;}
            if(max == null && startDate >= min) {return true;}
            if (startDate <= max && startDate >= min) { return true; }
            return false;
        }
        );

       
            $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            var table = $('#order-listing-in').DataTable();

            // Event listener to the two range filtering inputs to redraw on input
            $('#min, #max').change(function () {
                table.draw();
            });
  });
})(jQuery);