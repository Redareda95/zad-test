const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)))

const validateFileSize = (params) => {
  console.log((params.file.size/1024)/1024)
  if(!params.hasError && (params.file.size/1024)/1024 > 4) {
    return {
      ...params,
      hasError: true,
      errorMessage: "Maximum file size exceeded. Size limit is 4MB."
    }
  }
  return params;
}

const validateFileType = (params) => {
  let types = ["jpeg", "jpg", "png", "webp", "gif", "bmp"], acceptedTypes = new RegExp(types.join("|"))
  
  if(!params.hasError && !acceptedTypes.test(params.file.type)) {
    return {
      ...params,
      hasError: true,
      errorMessage: `Incorrect file type. Please try one of the following: ${types.join(',')}`
    }
  }
  return params;
}

const readFile = (params) => {
  if(!params.hasError)  {
    let reader = new FileReader();

    reader.onload = function(e) {
      var img = new Image();
      img.src = e.target.result;
      params.destination.appendChild(img);
    };
    reader.readAsDataURL(params.file);
  }
  return params;
}

const handleError = (params) => {
  if(params.hasError) {
    params.errorContainer.innerHTML = params.errorMessage;
  }
  return params;
}

function handleChange(e) {
  const errorContainer = document.querySelector('.error'), files = e.target.files, destination = document.querySelector('.results');
  errorContainer.innerHTML = '';
    destination.innerHTML = '';
  
    Object.keys(files).map(i => {
      // let file = files[i];
      let params = {
        file: files[i],
        hasError: false,
        errorMessage: '',
        destination,
        errorContainer
      }
      
      let validateFile = compose(
        readFile,
        handleError,
        validateFileType,
        validateFileSize
      )
      
      validateFile(params);
  })
}

const imagesSVG = '<img src="/addimage.gif" alt="add new ad form">'

document.querySelector('.svg').innerHTML = imagesSVG; 

