(function($) {
  'use strict';
  if ($("#fileuploader").length) {
    $("#fileuploader").uploadFile({
      url: "/uploads/",
      fileName: "myfile"
    });
  }
})(jQuery);