<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
*@description to open Project's Documentaction
*/
Route::get('/docs', 'DocsController@index');

 /**
 * @description To Clear Cache of Application
 */
	Route::get('/clear-cache', 'CacheController@index');


 /**
 * @description Show the home page of the site
 */
	Route::get('/', 'Website\FrontController@index');


 /**
 * @description Show Full Profile For User With Portfolio
 * @param id ID of the user in database
 */	
	Route::get('/user/{id}', 'Website\FrontController@show');

/**
 * @description Login System which redirects to either admin panel or user's profile, using  middleware CheckRole.php
 */
	Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('/login', 'Auth\LoginController@login');

 /**
 * @description Registry as normal user (use role user)
 */
	Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
	Route::post('/register', 'Auth\RegisterController@register');    
  

/**
 * @description admin group route which is viewed after admin is login, using middleware CheckRole.php which inherits middleware Authenticate.php
 */
Route::group(['prefix' => 'admin', 'middleware'=>'roles', 'roles'=>'admin'], function(){
	
	/**
 	* @description Show the home page of the admin panel
 	*/
	  Route::get('/home', 'Admin\RolesController@showPanel');

	  /**
 		* @description Roles Control to Convert permissions
 	  */
	  Route::get('/user-roles', 'Admin\RolesController@showUsers');
	  Route::post('/add-roles', 'Admin\RolesController@addRole');

	  /**
		 * @index_admins Show all user with role admin view
		 * @register_admin Show the view to create a user with role admin
		 * @add_admin Save user with role admin in database
		 * @index_user Show all user - with role user view
		 * @register_user Show the view to create a userwith role user
		 * @add_user Save user - with role user in database
		 * 
		 * @edit Show the view to edit user with anyrole info (using roles to hide or show extra information needed for role=user).
		 * @update user with anyrole data in database.
		 * @destroy Delete a user with anyrole from database
	 */
	  Route::get('/admins', 'Admin\UsersController@index_admins');
	  Route::get('/add-admin', 'Admin\UsersController@register_admin');
	  Route::post('/add-admin', 'Admin\UsersController@add_admin');
	  Route::get('/users/all', 'Admin\UsersController@index_user');
	  Route::get('/add-user', 'Admin\UsersController@register_user');
	  Route::post('/add-user', 'Admin\UsersController@add_user');

	  // Edit & Delete Users (With any Role)
	  Route::get('/edit-user/{id}', 'Admin\UsersController@edit');
	  Route::patch('/edit-user/{id}', 'Admin\UsersController@update');
	  Route::post('/delete-user/{id}', 'Admin\UsersController@destroy');

	  /**
	 * @index Show the main view
	 * @create Show the view to create a portfolio
	 * @store Save a portfolio in database to specific user
	 * @edit Show the view to edit a portfolios
	 * @update Update portfolios data in database
	 * @destroy Delete a portfolios in database
	 */
	  Route::get('/all-portfolios/{id}', 'Admin\PortfolioController@index');
	  Route::get('/add-portfolio-to-user/{id}', 'Admin\PortfolioController@create');
	  Route::post('/add-portfolio-to-user/{id}', 'Admin\PortfolioController@store');
	  Route::get('/edit-portfolio/{id}', 'Admin\PortfolioController@edit');
	  Route::patch('/edit-portfolio/{id}', 'Admin\PortfolioController@update');
	  Route::post('/delete-portfolio/{id}', 'Admin\PortfolioController@destroy');

});

/**
 * @description For All Roles Access
*/
Route::group(['middleware'=>'roles', 'roles'=>['admin', 'user']], function(){

  Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

});

/**
 * @description Normal User Access
*/
Route::group(['middleware'=>'roles', 'roles'=>['admin', 'user']], function(){

 
/**
 * @description User's Profile Update Info Route
 */
  Route::get('/my-profile', 'Users\ProfileController@edit');
  Route::patch('/my-profile', 'Users\ProfileController@update');
  
  /**
 * @index Show the main view
 * @create Show the view to create a portfolio for auth user
 * @store Save a portfolio for auth user in database
 * @edit Show the view to edit a portfolio for auth user
 * @update Update portfolio for auth user data in database
 * @destroy Delete a portfolio for auth user in database
 */
  Route::get('/add-portfolio', 'Users\PortfolioController@create');
  Route::post('/add-portfolio', 'Users\PortfolioController@store');
  Route::get('/my-portfolio/all', 'Users\PortfolioController@index');
  Route::post('/delete-portfolio/{id}', 'Users\PortfolioController@destroy');
  Route::get('/delete-portfolio/{id}', 'Users\PortfolioController@index');
  Route::get('/my-portfolio/edit-portfolio/{id}', 'Users\PortfolioController@edit');
  Route::patch('/my-portfolio/edit-portfolio/{id}', 'Users\PortfolioController@update');

});

