 - Laravel Version: 5.8
 - PHP Version: 7.1.3
 - Database Driver & Version: MySQL/MariaDB 10.0.31

## Description

- User's Management Application, where it has two roles,

    1) is the **Admin** which has full access, to Create, Read, Update &amp; Delete all data of other users in the System.

    2) is the **User** which only can CRUD his own data after authentication to the system.

- The Purpose of this application, is the interaction of user's upon the service by filling up their personal information. 
e.g uploading their profile picture, writing brief description about themselves, &amp; giving extra information like social links, or contact information.

- The Admin can control content entry of the site, and can give or change permissions  upon users registered in the system. 

## User Manual

This Application is Built with Laravel Framewirk, which depends mainly On MVC Design Pattern, Used in this application MySQL as a DB, Auth Facades for user's authentication.

In the Coming Few Lines I will explain how to setup this Application on your PC.

- 1 - Setting Up Laravel Environment.
- 2 - Cloning the Repo into your PC.
- 3 - Setting Up DB Connection and tables.
- 4 - Setting Up Application Key.
- 5 - Migrating & Seeding The DB.
- 6 - Running & Serving The Application.

## 1 - Setting Up Laravel Environment

Firstly You need to set up Laravel's Environment to deploy this Application, So after Installing The Apache Server Or Any PHP Compiler, 
Make Sure You Downloaded &amp; Setup composer from <a href="https://getcomposer.org/download/">here</a>. As Laravel utilizes Composer to manage its dependencies.

For More Details Visit <a href="https://laravel.com/docs/5.8/installation">Laravel Docs</a>

## 2 - Cloning the Repo into your PC.

With Basic Knowledge in Git, we are going to Clone the Repository into your PC

- **First Step,**

    Open The Command Prompet in the prefered distination you want the application to be in, 

- **Second Step,**
    
    Copy this line into the CMD, &amp; press Enter
    
    <code>git clone https://gitlab.com/Redareda95/zad-test</code>

- **Third Step,**

    After Cloning, Step forward into the project's folder.
    
    By Writing this line on CMD:
    
    <code>cd zad-test</code>
    
- **Firth Step,**
    
    Update the composer vendor files, By  writing this command in CMD:

    <code>composer update</code>
    
## 3 - Setting Up DB Connection and tables.
    
Now We need to create the DB manually, in our DB System, Weither its By a DB Management System Like PHPMyAdmin, or any other way.
    
- Name the DB **zad**.
- Create a file in the root folder of project named **.env**.
- Copy Content of **.env.example** to **.env** file you created.
    <br>
    <code>P.S</code>:
    You may need to change those config lines
    <code>
    DB_DATABASE=zad
    DB_USERNAME=root
    DB_PASSWORD=
    </code>
    <b>Upon your DB credintials.</b>


## 4 - Setting Up Application Key.

Write this command in your CMD:

<code>php artisan key:generate</code>

Your Application Key will be generated Successfully.

##  5 - Migrating & Seeding The DB.

We are almostly done configuring the App to be succesfully run.

But Our DB is empty of tables so we need to fill it up.

- **First,**

Run this command on your CMD:

<code>php artisan migrate</code>

- **Secondly,**

For DB Seeding with real data, run this command on CMD:

<code>php artisan db:seed</code>

##  6 - Running & Serving The Application.

Now You are Ready to use My User's Management System.

Just write this line on your CMD:

<code>php artisan serve</code>

Then go to this route, by either writing <code>/login</code> after the route in your browser, or by clicking on login button on Navbar.

- **Admin Credintials to login to managment panel**
- email: reda19952013@gmail.com
- password: 123456

## To Clear Cache of Application

Just add this URL after main route of Application

<code>/clear-cache</code>

And enjoy a cache free application.

## To View Application Routes Documentation

Just add this URL after main route of Application

<code>/docs</code>

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
