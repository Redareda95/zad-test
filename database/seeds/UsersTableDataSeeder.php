<?php

use Illuminate\Database\Seeder;

class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ahmed Reda',
                'email' => 'reda19952013@gmail.com',
	            'password' => bcrypt('123456'),
                'remember_token' => 'ggItpjiDtNAJ9TmNGTWlh5B5nQX9VYU79yHaBRggDLj3LJ9TIuExpEcSDRUo',
                'created_at' => '2019-04-11 09:46:28',
                'updated_at' => '2019-07-11 14:05:00',
            )
        ));


    }
}
