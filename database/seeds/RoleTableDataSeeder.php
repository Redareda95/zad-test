<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       Role::create([
	            'name' => 'admin',
	            'desc' => 'Have All Access',
	        ]); 
      Role::create([
                'name' => 'user',
                'desc' => 'Can Onlu Update his profile info and portofolio',
            ]); 
    }
}
