<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Portfolio extends Model
{
	protected $fillable = [
        'user_id', 'title', 'details', 'image'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
