<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Role::where('name', 'user')->first();

        $users = $query->users;
// dd($users[0]->details);
        /*
            * This Collection is sent to view to change meta data dynamically.
            *
            * Best Practice for this method is to create for it an independant model with its CRUD Functions to easily access and control meta data
        */

        $seo = (object) collect([
                        'title' => 'Homepage :: Zwaar',
                        'keywords' => 'test, new, company',
                        'desc' => "an user's managment system, to upload and show experience",
                        'author' => 'Ahmed Reda',
                ])->all();

        return view('website.welcome', compact('users', 'seo'));
    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = User::findOrFail($id);

        $portfolio = $query->portfolios;

        $seo = (object) collect([
                        'title' => $query->name . 'Profile :: Zwaar',
                        'keywords' => 'test, new, company',
                        'desc' => "an user's managment system, to upload and show experience",
                        'author' => 'Ahmed Reda',
                ])->all();
        
        //Check is the visitor is the profile owner or no
        if(\Auth::check())
        {
            if($id == auth()->user()->id)
            {
                return redirect('/my-portfolio/all');
            }
        }
       return view('website.profile', compact('portfolio', 'seo', 'query'));

    }

}
