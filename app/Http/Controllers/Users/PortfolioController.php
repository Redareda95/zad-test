<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Portfolio;

class PortfolioController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
	{
    	$seo = (object) collect([
                        'title' => 'My Posts :: Zwaar',
                        'keywords' => 'test, new, company',
                        'desc' => "an user's managment system, to upload and show experience",
                        'author' => 'Ahmed Reda',
                ])->all();

    	$id = \Auth::user()->id;

    	$portfolio = Portfolio::where('user_id', $id)->get();

    	return view('users.portfolio.portfolio', compact('seo', 'portfolio'));
	}

    public function create()
    {

    	$seo = (object) collect([
                        'title' => 'Add new portfolio :: Zwaar',
                        'keywords' => 'test, new, company',
                        'desc' => "an user's managment system, to upload and show experience",
                        'author' => 'Ahmed Reda',
                ])->all();

    	return view('users.portfolio.add', compact('seo'));
    }

    public function store(Request $request)
    {
        $this->validate(request(),[
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
		
    	$id = \Auth::user()->id;

		$img_name = time() . '.' . $request->image->getClientOriginalExtension();

        $portfolio = new Portfolio;

        $portfolio->user_id = $id;

        $portfolio->image = $img_name;

        $portfolio->save();

        $request->image->move('uploads', $img_name);

        session()->flash('message', 'Portfolio Is Added To Your Profile');

        return redirect('/my-portfolio/all');

    }

    

    public function edit($id)
    {
        $portfolio = Portfolio::findOrFail($id);

        if($portfolio->user_id == \Auth::user()->id)
        {
            $seo = (object) collect([
                        'title' => 'Edit ' . $portfolio->title . ' Details :: Zwaar',
                        'keywords' => 'test, new, company',
                        'desc' => "an user's managment system, to upload and show experience",
                        'author' => 'Ahmed Reda',
                    ])->all();

            return view('users.portfolio.edit', compact('portfolio', 'seo'));
        
        }
        
        session()->flash('message', "Can not Edit Portfolio");

        return back();
    }
    
    public function update(Request $request, $id)
    {
    	$portfolio = Portfolio::findOrFail($id);

        $this->validate(request(),[
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if($portfolio->user_id != \Auth::user()->id)
        {
           session()->flash('message', "Can not Edit Portfolio");

           return back();
        }

        if(file_exists($request->file('image')))
        {
           $img_name = time() . '.' . $request->image->getClientOriginalExtension();

           Portfolio::where('id', $id)->update(['image' => $img_name]);
                     
           $request->image->move('uploads', $img_name);

        }

        Portfolio::where('id', $id)->update($request->except(['_token', '_method', 'image']));

        session()->flash('message', 'Portfolio Is Updated');

        return redirect('/my-portfolio/all');
    }

    public function destroy($id)
    {
        $portfolio = Portfolio::findOrFail($id);
        
        if($portfolio->user_id == \Auth::user()->id)
        {
            $portfolio->delete();

            session()->flash('message', 'Portfolio is Deleted');

            return back();
        }
            
       session()->flash('message', "Can not delete Portfolio");

       return back();
    }
}
