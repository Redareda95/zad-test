<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserDetail;

class ProfileController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        $seo = (object) collect([
                        'title' => \Auth::user()->name . " Profile :: Zwaar",
                        'keywords' => 'test, new, company',
                        'desc' => "an user's managment system, to upload and show experience",
                        'author' => 'Ahmed Reda',
                ])->all();

    	if (is_null(\Auth::user()->details)) 
    	{
    		return back();
    	}
    	
		return view('users.profile.edit', compact('seo'));	
    }

    public function update(Request $request)
    {
    	$id = \Auth::user()->id;

    	$this->validate(request(),[
            'name' => 'required|max:191',
            'email' => 'required|email|max:191|unique:users,email,'.$id,
            'desc'=>'required',
            'logo'=>'image|mimes:jpg,jpeg,png,gif|max:2048' 
        ]);

		User::where('id', $id)->update([
											'name' => $request->name,
											'email' => $request->email,
										]);

		// Case logo Changed
		
		if(file_exists($request->file('logo')))
        {
           $img_name = time() . '.' . $request->logo->getClientOriginalExtension();

           UserDetail::where('user_id', $id)->update(['logo'=>$img_name]);
                     
           $request->logo->move('uploads', $img_name);

        }

         UserDetail::where('user_id', $id)->update([
         												'desc' => $request->desc,
         												'extra_details' => $request->extra_details,
         											]);

        // Case Password Changed
         
    	if (isset($request->password)) 
        {
            $this->validate(request(),[
              'password' => 'required|min:6|confirmed',
            ]);

            $password = request(['password']);
            
            User::where('id', $id)->update(['password' => bcrypt($password['password'])]);

        }

		session()->flash('message', 'Your Informations Is Updated');

        return redirect('/my-profile');

    } 
}
