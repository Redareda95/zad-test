<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CacheController extends Controller
{
    public function index()
    {
    	\Artisan::call('cache:clear');
	    
	    session()->flash('message', 'Cache Is Cleared');

	    return redirect('/');
    }
}
