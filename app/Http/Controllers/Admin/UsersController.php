<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserDetail;
use App\Role;
use Illuminate\Support\Facades\Hash;
use DB;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index_admins()
    {
        $query = Role::where('name', 'admin')->first();

        $admins = $query->users;

        return view('admin.admins.admins', compact('admins'));
    }

    public function register_admin()
    {
        return view('admin.admins.add');
    }

    public function add_admin(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required',
            'email'=>'required|email|unique:users',
            'password'=>'required|string|min:6|confirmed',
        ]);

        $pass = $request->password;

        $admin = new User;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = Hash::make($pass);
        $admin->save();

        $role = DB::table('user_role')->insert(
                                                ['user_id' => $admin->id, 'role_id' => 1]
                                            );

        session()->flash('message', 'Admin Is Created');

        return redirect('/admin/admins');
    }

    public function edit($id)
    {
    	$user = User::find($id);

    	return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, $id) 
    {
    	$this->validate(request(),[
            'name' => 'required',
            'email'=>'required|email|unique:users,email,'.$id,
        ]);

        User::where('id', $id)->update([
                                        'name' => $request->name, 
                                        'email' => $request->email,
                                    ]);

        if (isset($request->password)) 
        {
            $this->validate(request(),[
            'password' => 'required|min:6|confirmed',
            ]);

            $password = request(['password']);
            
            User::where('id', $id)->update(['password' => bcrypt($password['password'])]);

        }

        /*
        	* $request->withDetails
        	* Hidden Input Sent to Determine Weither the user has details in user_details table or no
        */

        if(isset($request->withDetails))
        {
        	$this->validate(request(),[
	            'desc'=>'required',
	            'logo'=>'image|mimes:jpg,jpeg,png,gif|max:2048'      
	        ]);

            if(file_exists($request->file('logo')))
	        {
	           $img_name = time() . '.' . $request->logo->getClientOriginalExtension();

	           UserDetail::where('user_id', $id)->update(['logo'=>$img_name]);
	                     
	           $request->logo->move('uploads', $img_name);

	        }

	         UserDetail::where('user_id', $id)->update([
	         												'desc' => $request->desc,
	         												'extra_details' => $request->extra_details,
	         											]);
        }

        session()->flash('message', 'User Is Updated');

        return redirect('/admin/user-roles');

    }

    public function index_user()
    {
        
        $query = Role::where('name', 'user')->first();

        $users = $query->users;

        return view('admin.users.users', compact('users'));
    }

    public function register_user()
    {
        return view('admin.users.add');
    }

    public function add_user(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required',
            'email'=>'required|email|unique:users',
            'password'=>'required|string|min:6|confirmed',
            'desc' => 'required',
            'logo'=>'required|image|mimes:jpg,jpeg,png,gif|max:2048'      
        ]);

        $img_name = time() . '.' . $request->logo->getClientOriginalExtension();

        $pass = $request->password;

        $user = new User;

        $user->name = $request->name;

        $user->email = $request->email;

        $user->password = Hash::make($pass);

        $user->save();

 		$user->details()->create([

			     	 'desc' => $request->desc,

			     	 'logo' => $img_name,

			     	 'extra_details' => $request->extra_details,

		     	 ]);

	 	$request->logo->move('uploads', $img_name);

        $role = DB::table('user_role')->insert(
                                                ['user_id' => $user->id, 'role_id' => 2]
                                            );

        session()->flash('message', 'User Is Created');

        return redirect('/admin/users/all');
    }

    public function destroy($id)
    {
    	$user = User::findOrFail($id);

        $user->delete();

        session()->flash('deleted', 'User is Deleted');

        return back();
    }
}
