<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Portfolio;

class PortfolioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
    	$user = User::where('id', $id)->first();

    	$portfolio = $user->portfolios;

    	if(count($portfolio) == 0)
    	{
    		return redirect('/admin/users/all');
    	}

    	return view('admin.portfolio.portfolio', compact('portfolio', 'user'));
    }

    public function edit($id)
    {
        $portfolio = Portfolio::findOrFail($id);

        return view('admin.portfolio.edit', compact('portfolio'));
    }

    public function create($id)
    {
    	$user = User::findOrFail($id);

    	return view('admin.portfolio.add', compact('user'));
    }

    public function store(Request $request, $id)
    {
        $this->validate(request(),[
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
		
		$img_name = time() . '.' . $request->image->getClientOriginalExtension();

        $portfolio = new Portfolio;

        $portfolio->user_id = $id;

        $portfolio->image = $img_name;

        $portfolio->save();

        $request->image->move('uploads', $img_name);

        session()->flash('message', 'Portfolio Is Created to User');

        return redirect('/admin/all-portfolios/' . $id);

    }

    public function update(Request $request, $id)
    {
        $this->validate(request(),[
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $portfolio = Portfolio::findOrFail($id);

        if(file_exists($request->file('image')))
        {
           $img_name = time() . '.' . $request->image->getClientOriginalExtension();

           Portfolio::where('id', $id)->update(['image' => $img_name]);
                     
           $request->image->move('uploads', $img_name);

        }

        Portfolio::where('id', $id)->update($request->except(['_token', '_method', 'image']));

        session()->flash('message', 'Portfolio Is Updated');

        return redirect('/admin/all-portfolios/' . $portfolio->user_id);
    }

    public function destroy($id)
    {
        $product = Portfolio::findOrFail($id);

        $product->delete();

        session()->flash('deleted', 'Portfolio is Deleted');

        return back();
    }
}
