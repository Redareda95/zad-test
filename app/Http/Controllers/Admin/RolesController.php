<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function showPanel()
    {
    	return view('admin.home');
    }
    
    public function showUsers()	
    {
    	$users = User::get();

        return view('admin.users.showUsers', compact('users'));
    }

    public function addRole(Request $request)
    {
    	$user = User::where('email', $request['email'])->first();

        $user->roles()->detach();

        if($request['admin'])
        {
            $user->roles()->attach(Role::where('name', 'admin')->first());
        }

        if($request['user'])
        {
            $user->roles()->attach(Role::where('name', 'user')->first());			
        }

        return redirect()->back();
    }
}
