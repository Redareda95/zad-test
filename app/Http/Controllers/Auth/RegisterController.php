<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegistrationForm()
    {
        $seo = (object) collect([
                        'title' => 'Login / Register :: ZadPortofolios',
                        'keywords' => 'test, new, company',
                        'desc' => "an user's managment system, to upload and show experience",
                        'author' => 'Ahmed Reda',
                ])->all();

        return view('users.auth.reg', compact('seo'));
    }

    public function register(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'desc' => 'required',
            'logo'=>'required|image|mimes:jpg,jpeg,png,gif|max:2048'
        ]);

        $password = request(['password']);

        $img_name = time() . '.' . $request->logo->getClientOriginalExtension();

        $user = new User;

        $user->name = request('name');

        $user->email = request('email');

        $user->password = bcrypt($password['password']);

        $user->save();

        $user->details()->create([

                     'desc' => $request->desc,

                     'logo' => $img_name,

                     'extra_details' => $request->extra_details,

                 ]);

        $request->logo->move('uploads', $img_name);

        $role = DB::table('user_role')->insert(
                                                ['user_id' => $user->id, 'role_id' => 2]
                                            );

        $this->guard()->login($user);

        session()->flash('message', 'Your Account Is Created');

        return redirect('/');

    }
}
