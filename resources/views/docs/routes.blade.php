<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Routes</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.13.3/swagger-ui.css">
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(document).on('click', '.opblock-summary', function(){
                    $(this).closest('.opblock').find('.opblock-body').slideToggle("slow");
                });
            });
        </script>

    </head>
    <body>
        <section id="routes" class="doc-section swagger-ui">
            <h2 class="section-title">Route documentation</h2>
            <div class="section-block">
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>api/user</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> Closure</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> api,auth:api </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>/</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Show the home page of the site</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Website\FrontController@index</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>docs</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">to open Project&#039;s Documentaction</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\DocsController@index</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>clear-cache</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">To Clear Cache of Application</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\CacheController@index</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>user/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Show Full Profile For User With Portfolio</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Website\FrontController@show</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="table-container">
                                        <table class="parameters">
                                            <thead>
                                            <tr>
                                                <th class="col col_header parameters-col_name">Parameter name</th>
                                                <th class="col col_header parameters-col_controller">Description</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                                                                    <tr>
                                                        <td class="col parameters-col_name">
                                                            <div class="markdown">id </div>
                                                        </td>
                                                        <td class="col parameters-col_controller">
                                                            <div class="markdown"> ID of the user in database</div>
                                                        </td>
                                                    </tr>
                                                                                            </tbody>
                                        </table>
                                    </div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>register</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Registry as normal user (use role user)</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">register</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Auth\RegisterController@showRegistrationForm</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,guest </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>register</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Registry as normal user (use role user)</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Auth\RegisterController@register</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,guest </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>login</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Login System which redirects to either admin panel or user&#039;s profile, using  middleware CheckRole.php</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">login</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Auth\LoginController@showLoginForm</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,guest </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>login</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Login System which redirects to either admin panel or user&#039;s profile, using  middleware CheckRole.php</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Auth\LoginController@login</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,guest </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>logout</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">For All Roles Access</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">logout</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Auth\LoginController@logout</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/add-portfolio-to-user/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\PortfolioController@store</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/add-admin</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@add_admin</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-">
                        <div class="opblock-summary opblock-summary-">
                            <span class="opblock-summary-method">PATCH</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>my-profile</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">User&#039;s Profile Update Info Route</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\ProfileController@update</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>my-profile</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">User&#039;s Profile Update Info Route</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\ProfileController@edit</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-">
                        <div class="opblock-summary opblock-summary-">
                            <span class="opblock-summary-method">PATCH</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>my-portfolio/edit-portfolio/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\PortfolioController@update</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>my-portfolio/edit-portfolio/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\PortfolioController@edit</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>my-portfolio/all</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\PortfolioController@index</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>add-portfolio</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\PortfolioController@create</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>delete-portfolio/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\PortfolioController@index</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>delete-portfolio/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\PortfolioController@destroy</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/add-admin</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@register_admin</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/add-portfolio-to-user/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\PortfolioController@create</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/users/all</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@index_user</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/user-roles</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Roles Control to Convert permissions</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\RolesController@showUsers</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>add-portfolio</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Users\PortfolioController@store</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/edit-user/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@edit</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-">
                        <div class="opblock-summary opblock-summary-">
                            <span class="opblock-summary-method">PATCH</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/edit-user/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@update</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-">
                        <div class="opblock-summary opblock-summary-">
                            <span class="opblock-summary-method">PATCH</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/edit-portfolio/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\PortfolioController@update</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/edit-portfolio/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\PortfolioController@edit</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/delete-user/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@destroy</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/delete-portfolio/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\PortfolioController@destroy</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/all-portfolios/{id}</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\PortfolioController@index</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/admins</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@index_admins</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/add-user</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@add_user</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/add-user</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description"></div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\UsersController@register_user</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-post">
                        <div class="opblock-summary opblock-summary-post">
                            <span class="opblock-summary-method">POST</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/add-roles</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Roles Control to Convert permissions</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\RolesController@addRole</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                                    <div class="opblock opblock-get">
                        <div class="opblock-summary opblock-summary-get">
                            <span class="opblock-summary-method">GET|HEAD</span>
                            <span class="opblock-summary-path">
                                                            <a class="nostyle">
                                                                <span>admin/home</span>
                                                            </a>
                                                            <img src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI0IDI0IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNCAyNCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGQ9Ik0xOSA3djRINS44M2wzLjU4LTMuNTlMOCA2bC02IDYgNiA2IDEuNDEtMS40MUw1LjgzIDEzSDIxVjd6Ii8+Cjwvc3ZnPgo=" class="view-line-link">
                                                        </span> <!-- opblock-summary-path -->
                            <div class="opblock-summary-description">Show the home page of the admin panel</div>
                            <button class="authorization__btn unlocked" aria-label="authorization button unlocked">
                                <svg width="20" height="20">
                                    <use href="#unlocked" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use>
                                </svg>
                            </button>
                        </div> <!-- opblock-summary -->
                        <div class="opblock-body" style="display: none;"><!-- react-text: 1914 --><!-- /react-text -->
                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Information</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                <div class="table-container">
                                    <table class="parameters">
                                        <thead>
                                        <tr>
                                            <th class="col col_header parameters-col_name">Route name</th>
                                            <th class="col col_header parameters-col_controller">Controller</th>
                                            <th class="col col_header parameters-col_middleware">Middleware</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="col parameters-col_name">
                                                <div class="markdown">Nameless</div>
                                            </td>
                                            <td class="col parameters-col_controller">
                                                <div class="markdown"> App\Http\Controllers\Admin\RolesController@showPanel</div>
                                            </td>
                                            <td class="col parameters-col_middleware">
                                                <div class="markdown"> web,roles,auth </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div> <!-- table-container -->
                            </div> <!-- opblock-section -->

                            <div class="opblock-section">
                                <div class="opblock-section-header">
                                    <div class="tab-header">
                                        <h4 class="opblock-title">Parameters</h4>
                                    </div>
                                </div> <!-- opblock-section-header -->
                                                                    <div class="opblock-description-wrapper"><p>Parameters</p></div>
                                                            </div> <!-- opblock-section -->
                        </div> <!-- opblock-body -->
                    </div> <!-- opblock -->
                            </div> <!-- opblock -->
        </section><!--//doc-section-->
    </body>
</html>

