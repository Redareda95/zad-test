@extends('admin.master')
@section('content')

<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          
@if($flash = session('message'))
  <div class="alert alert-success" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
@if($flash = session('deleted'))
  <div class="alert alert-danger" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
          <h4 class="card-title">System's Admins <a href="/admin/add-admin" style="float: right"><button class="btn btn-info"><i class="fa fa-plus"></i> Add admin</button></a></h4>
          <p class="card-description">
            Has all <code>access</code>
          </p>
          <div class="table-responsive">
            <table class="table table-bordered" id="order-listing">
              <thead>
                <tr>
                  <th>
                    #
                  </th>
                  <th>
                    Name
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Last Login
                  </th>
                  <th>
                    Options
                  </th>
                </tr>
              </thead>
              <tbody>
              	@foreach($admins as $admin)
                <tr>
                  <td>{{ $admin->id }}</td>
                  <td>{{ $admin->name }}</td>
                  <td>{{ $admin->email }}</td>
                  <td>{{ $admin->updated_at->diffForHumans() }}</td>
                  <td>
                  	<a href="/admin/edit-user/{{ $admin->id }}"><button type="button" class="btn btn-icons btn-rounded btn-outline-success"><i class="mdi mdi-tooltip-edit"></i></button></a>
                    @if($admin->id != auth()->user()->id)
                  	<form action="/admin/delete-user/{{ $admin->id }}" method="POST" style="display:inline!important">
                      @csrf
                      <button type="submit" class="btn btn-icons btn-rounded btn-outline-warning" onclick="if (!confirm('Are you sure you want to delete?')) { return false }"><i class="mdi mdi-delete"></i></button>
                    </form>
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Last Login</th>
                  <th>Options</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
