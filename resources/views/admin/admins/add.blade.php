@extends('admin.master')
@section('content')

<div class="col-12 stretch-card grid-margin">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Add new System Admin</h4>
      <p class="card-description">
        Has all  access
      </p>
      <form class="forms-sample" method="POST" action="/admin/add-admin">
        @csrf
        <div class="form-group row">
          <label for="exampleInputEmail5" class="col-sm-3 col-form-label">Name *</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="exampleInputEmail6" placeholder="Enter Username" name="name">
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Email * <small>must be unique</small></label>
          <div class="col-sm-9">
            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email" name="email">
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Password</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" name="password">
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Confirm Password</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm Password" name="password_confirmation">
          </div>
        </div>
@if(count($errors))
<div class="alert alert-danger">
<ul>
@foreach($errors->all() as $error)
    <li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
        <button type="submit" class="btn btn-success mr-2">Done</button>
      </form>
    </div>
  </div>
</div>
@endsection
