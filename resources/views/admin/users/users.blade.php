@extends('admin.master')
@section('content')

<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          
@if($flash = session('message'))
  <div class="alert alert-success" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
@if($flash = session('deleted'))
  <div class="alert alert-danger" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
          <h4 class="card-title">System's Registered Users <a href="/admin/add-user" style="float: right"><button class="btn btn-info"><i class="fa fa-plus"></i> Add User</button></a></h4>
          <p class="card-description">
            Can Add, Edit, Delete There Own Portofolio Only Normal <code>access</code>
          </p>
          <div class="table-responsive">
            <table class="table table-bordered" id="order-listing">
              <thead>
                <tr>
                  <th>
                    #
                  </th>
                  <th>
                    Name
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Last Login
                  </th>
                  <th>
                    Portfolio
                  </th>
                  <th>
                    Options
                  </th>
                </tr>
              </thead>
              <tbody>
              	@foreach($users as $user)
                <tr>
                  <td>{{ $user->id }}</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->updated_at->diffForHumans() }}</td>
                  <td>
                    @if(count($user->portfolios) > 0)
                      <a href="/admin/all-portfolios/{{ $user->id }}">
                        <button type="button" class="btn btn-outline-primary"><i class="mdi mdi-eye"> View Portfolio</i></button>
                      </a>
                    @endif
                    <a href="/admin/add-portfolio-to-user/{{ $user->id }}">
                      <button type="button" class="btn btn-outline-success"><i class="mdi mdi-plus"> Add Portfolio to user</i></button>
                    </a>
                  </td>
                  <td>
                  	<a href="/admin/edit-user/{{ $user->id }}"><button type="button" class="btn btn-icons btn-rounded btn-outline-success"><i class="mdi mdi-tooltip-edit"></i></button></a>
                  	<form action="/admin/delete-user/{{ $user->id }}" method="POST" style="display:inline!important">
                      @csrf
                      <button type="submit" class="btn btn-icons btn-rounded btn-outline-warning" onclick="if (!confirm('Are you sure you want to delete?')) { return false }"><i class="mdi mdi-delete"></i></button>
                    </form>
                    
                  </td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Last Login</th>
                  <th>Portfolio</th>
                  <th>Options</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
