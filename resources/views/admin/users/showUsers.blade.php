@extends('admin.master')
@section('content')

<div class="card">
	<div class="card-body">
	  <h4 class="card-title"><i class="fa fa-users"></i> Users & Roles</h2></h4>
@if($flash = session('message'))
  <div class="alert alert-success" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
@if($flash = session('deleted'))
  <div class="alert alert-danger" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
  	<div class="row">
    	<div class="col-12 table-responsive">
	      <table id="order-listing2" class="table" style="width: 100%;">
	        <thead>
		        <tr>
				<th>id</th>
				<th>Name</th>
				<th>Email</th>
				<th>Created At</th>
				<th>Admin</th>
				<th>User</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				@if($user->id != auth()->user()->id)
				<form method="POST" action="/admin/add-roles">
					{{ csrf_field() }}
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->created_at->toFormattedDateString() }}</td>
					<td>
						<input type="hidden" name="email" value="{{ $user->email }}">
						<input type="checkbox" name="admin" onchange="this.form.submit()" {{ $user->hasRole('admin')?'checked': ' ' }}>
					</td>
					<td>
						<input type="checkbox" name="user" onchange="this.form.submit()" {{ $user->hasRole('user')?'checked': ' ' }}>
					</td>
				</tr>
				</form>
				@endif
				@endforeach
	        </tbody>
	        <tfoot>
	        	<tr>
				<th>id</th>
				<th>Name</th>
				<th>Email</th>
				<th>Created At</th>
				<th>Admin</th>
				<th>User</th>
				</tr>
	        </tfoot>
	   	  </table>        
        </div>
      </div>
    </div>
  </div>
</div>
@endsection