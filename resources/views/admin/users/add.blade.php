@extends('admin.master')
@section('content')

<div class="col-12 stretch-card grid-margin">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Add New User</h4>
      <p class="card-description">
            Can Add, Edit, Delete There Own Portofolio Only Normal <code>access</code>
      </p>
      <form class="forms-sample" method="POST" action="/admin/add-user" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
          <label for="exampleInputEmail5" class="col-sm-3 col-form-label">Name *</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="exampleInputEmail6" placeholder="Enter Username" name="name">
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Email * <small>must be unique</small></label>
          <div class="col-sm-9">
            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email" name="email">
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Password</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" name="password">
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Confirm Password</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm Password" name="password_confirmation">
          </div>
        </div>

        <div>
          <div class="col-lg-6 form-group">
              <h4 class="mr-auto">Upload Profile Image</h4>
              <label class=newbtn>
                  <img id="blah" src="/profile.png" >
                  <input type="file" name="logo" class="form-control"  id="imgInp">
              </label>
            </div>
          <div class="col-lg-12 form-group" style="padding-bottom: 15px">
              <textarea class="form-control" name="desc" cols="92" rows="3" placeholder="Write down In Brief a Description about you"></textarea>
          </div>
          <div class="col-lg-12 form-group" style="padding-bottom: 15px">
              <textarea class="form-control" name="extra_details" cols="92" rows="3" placeholder="Add Any Extra Details about you, Social Links, Mobile Number ,Etc.."></textarea>
          </div>
        </div>

@if(count($errors))
<div class="alert alert-danger">
<ul>
@foreach($errors->all() as $error)
    <li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
        <button type="submit" class="btn btn-success mr-2">Done</button>
      </form>
    </div>
  </div>
</div>
@endsection
