@extends('admin.master')
@section('content')
<form method="POST" action="/admin/edit-user/{{ $user->id }}"  enctype="multipart/form-data">
    {{ csrf_field() }}
  <input type="hidden" name="_method" value="PATCH">
<div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit User Information</h3>
            </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label" for="inputSuccess"><i class="fa fa-user"></i> Name *<small>{{ $user->type }}</small></label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="name" id="inputSuccess" value="{{ $user->name }}">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label" for="inputSuccess"><i class="fa fa-at"></i> Email *</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="email" id="inputSuccess" value="{{ $user->email }}">
      </div>
    </div>
    <h4>If You need to change password <code>if not needed leave it empty</code></h4>
    <div class="form-group row">
      <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Password</label>
      <div class="col-sm-9">
        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" name="password">
      </div>
    </div>
    <div class="form-group row">
      <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Confirm Password</label>
      <div class="col-sm-9">
        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm Password" name="password_confirmation">
      </div>
    </div>
    @if($user->hasRole('user'))
    <input type="hidden" name="withDetails" value="withDetails">
    <div>
      <div class="col-lg-6 form-group">
          <h4 class="mr-auto">Upload Profile Image</h4>
          <label class=newbtn>
              <img id="blah" src="/uploads/{{ $user->details->logo }}" >
              <input type="file" name="logo" class="form-control"  id="imgInp">
          </label>
        </div>
      <div class="col-lg-12 form-group" style="padding-bottom: 15px">
          <textarea class="form-control" name="desc" cols="92" rows="3">{{ $user->details->desc }}</textarea>
      </div>
      <div class="col-lg-12 form-group" style="padding-bottom: 15px">
          <textarea class="form-control" name="extra_details" cols="92" rows="3">{{ $user->details->extra_details }}</textarea>
      </div>
    </div>
    <br>
    @endif
    <div class="row">
      <div class="col-sm-12">
        <div class="box-footer">
          <button type="submit" class="btn btn-success btn-block btn-flat">Edit User</button>
        </div>
      </div>
    </div>
</form>
@if(count($errors))
<div class="alert alert-danger">
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
@endif
@endsection
