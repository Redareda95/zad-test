<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Zwaar :: UserManagement Panel</title>
<!-- plugins:css -->
<link rel="stylesheet" href="\vendors\iconfonts\mdi\css\materialdesignicons.min.css">
<link rel="stylesheet" href="\vendors\css\vendor.bundle.base.css">
<link rel="stylesheet" href="\vendors\css\vendor.bundle.addons.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="/vendors/lightgallery/css/lightgallery.css">
  <link rel="stylesheet" type="text/css" href="/css/register.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">

<!-- endinject -->
<!-- plugin css for this page -->
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="\css\style.css">
<!-- endinject -->
<link rel="shortcut icon" href="\favicon.png">

</head>

<body style="height: auto;" onload="typeWriter()">
<div class="container-scroller">
<!-- partial:partials/_horizontal-navbar.html -->
<nav class="navbar horizontal-layout col-lg-12 col-12 p-0">
  <div class="container d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-top">
      <a class="navbar-brand brand-logo" href="/admin/home"><span style="color: black">Zwaar</span><span style="color: #0044cc;">Test</span></a>
      <a class="navbar-brand brand-logo-mini" href="/admin/home"><span style="color: black">Test</span><span style="color: #0044cc;">Zwaar</span></a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center">
      <form class="search-field ml-auto" action="#">
        {{-- <div class="form-group mb-0">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="mdi mdi-magnify"></i></span>
            </div>
            <input type="text" class="form-control">
          </div>
        </div> --}}
      </form>
      <ul class="navbar-nav navbar-nav-right mr-0">
        
       <li class="nav-item dropdown ml-4" style="visibility: hidden;">
          <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
            <i class="mdi mdi-bell-outline"></i>
            <span class="count bg-warning"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
            <a class="dropdown-item preview-item" href="#">
              <div class="preview-thumbnail">
                <div class="preview-icon bg-inverse-warning">
                  <i class="mdi mdi-layers mx-0"></i>
                </div>
              </div>
              <div class="preview-item-content">
                <h6 class="preview-subject font-weight-normal text-dark mb-1">Pending Orders</h6>
                <p class="font-weight-light small-text mb-0">
                  You have new Pending Orders.
                </p>
                <br>
                View All
              </div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item preview-item" href="#">
              <div class="preview-thumbnail">
                <div class="preview-icon bg-inverse-success">
                  <i class="mdi mdi-comment-text-outline mx-0"></i>
                </div>
              </div>
              <div class="preview-item-content">
                <h6 class="preview-subject font-weight-normal text-dark mb-1">New Mails</h6>
                <p class="font-weight-light small-text mb-0">
                  You have New Mails.
                </p>
                <br>
                View All
              </div>
            </a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
            <img class="img-xs rounded-circle" src="\images\faces-clipart\pic-2.png" alt="Profile image">
          </a>
          <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
            <a class="dropdown-item p-0">
              {{-- <div class="d-flex border-bottom w-100">
                <div class="py-3 px-4 d-flex align-items-center justify-content-center flex-grow-1"><i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i></div>
                <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right flex-grow-1"><i class="mdi mdi-account-outline mr-0 text-gray"></i></div>
                <div class="py-3 px-4 d-flex align-items-center justify-content-center flex-grow-1"><i class="mdi mdi-alarm-check mr-0 text-gray"></i></div>
              </div> --}}
            </a>
            <a class="dropdown-item">
              <i class="fa fa-user"></i> {{ Auth::user()->name }}
            </a>
            <a class="dropdown-item" href="/" target="_blank">
              <i class="fa fa-eye"></i> View Site
            </a>
            <a class="dropdown-item" href="/Stock/logout"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out-alt"></i> {{ __('Logout') }}
            </a>

            <form id="logout-form" action="/logout" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
        </li>
      </ul>
      <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
        <span class="mdi mdi-menu"></span>
      </button>
    </div>
  </div>
  <div class="nav-bottom">
    <div class="container">
      <ul class="nav page-navigation">
        <li class="nav-item">
          <a href="/admin/home" class="nav-link"><i class="link-icon mdi mdi-television"></i><span class="menu-title">DASHBOARD</span></a>
        </li>
        @if(Auth::user()->hasRole('admin'))
        <li class="nav-item">
          <a href="#" class="nav-link"><i class="link-icon mdi mdi-human"></i><span class="menu-title">System Users</span><i class="menu-arrow"></i></a>
          <div class="submenu">
            <ul class="submenu-item">
              <li class="nav-item"><a class="nav-link" href="/admin/user-roles">Users & Roles</a></li>
              <li class="nav-item"><a class="nav-link" href="/admin/admins">All Admins</a></li>
              <li class="nav-item"><a class="nav-link" href="/admin/users/all">All Users</a></li>
            </ul>
          </div>
        </li>
        @endif
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid page-body-wrapper" style="height: auto;">
      <div class="main-panel">
        <div class="content-wrapper">
