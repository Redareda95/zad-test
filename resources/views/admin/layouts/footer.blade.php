  </div>
        <!-- content-wrapper ends -->
        
 <!-- partial:partials/_footer.html -->
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="\vendors\js\vendor.bundle.base.js"></script>
  <script src="\vendors\js\vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="\js\template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="\js\dashboard.js"></script>
  <!-- End custom js for this page-->
  <script src="\js\editorDemo.js"></script>
  <script src="\js\formpickers.js"></script>
  <script src="\js\data-table.js"></script>
  <script src="/js/wizard.js"></script> 
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script>
var i = 0;
var txt = 'Welcome To Zwaar User Managment Panel.';
var speed = 50;

function typeWriter() {
  if (i < txt.length) {
    document.getElementById("demo").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}
</script>


<script>
    $(function() {
        $('#image-upload').change(function(){
            $("#oldPhotos").hide();
        });
    });
</script>
<script type="text/javascript">
  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});
</script>
<!-- for calculating price after discount -->

<!-- for exchange invoices -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>

<script type="text/javascript">
      $(".chosen").chosen({width: "100%"});
</script>

</body>

</html>
