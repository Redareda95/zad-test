@extends('admin.master')
@section('content')
<div class="row">
@if($flash = session('message'))
<div class="col-md-12">
  <div class="alert alert-success" role="alert">
    <b>{{ $flash }}</b>
  </div>  
</div>
@endif
@if($flash = session('deleted'))
<div class="col-md-12">
  <div class="alert alert-danger" role="alert">
    <b>{{ $flash }}</b>
  </div>  
</div>
@endif
  <div class="col-md-12">
      <h1 id="demo"></h1>
  </div>
  
</div>
@endsection