@extends('admin.master')
@section('content')

<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          
@if($flash = session('message'))
  <div class="alert alert-success" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
@if($flash = session('deleted'))
  <div class="alert alert-danger" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
          <h4 class="card-title">Full Portfolio For {{ $user->name }} <a href="/admin/add-portfolio-to-user/{{ $user->id }}" style="float: right"><button class="btn btn-info"><i class="fa fa-plus"></i> Add Portfolio to user</button></a></h4>
          <p class="card-description">
            Full Portfolio added By <code>{{ $user->name }} </code>
          </p>
          <div class="table-responsive">
            <table class="table table-bordered" id="order-listing">
              <thead>
                <tr>
                  <th>
                    #
                  </th>
                  <th>
                    Username
                  </th>
                  <th>
                    Portfolio Image
                  </th>
                  <th>
                    Created At
                  </th>
                  <th>
                    Options
                  </th>
                </tr>
              </thead>
              <tbody>
              	@foreach($portfolio as $port)
                <tr>
                  <td>{{ $port->id }}</td>
                  <td>{{ $port->user->name }}</td>
                  <td><img src="/uploads/{{ $port->image }}" width="75" class="img-responsive"></td>
                  <td>{{ $port->created_at->diffForHumans() }}</td>
                  <td>
                  	<a href="/admin/edit-portfolio/{{ $port->id }}"><button type="button" class="btn btn-icons btn-rounded btn-outline-success"><i class="mdi mdi-tooltip-edit"></i></button></a>
                  	<form action="/admin/delete-portfolio/{{ $port->id }}" method="POST" style="display:inline!important">
                      @csrf
                      <button type="submit" class="btn btn-icons btn-rounded btn-outline-warning" onclick="if (!confirm('Are you sure you want to delete?')) { return false }"><i class="mdi mdi-delete"></i></button>
                    </form>
                    
                  </td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>
                    #
                  </th>
                  <th>
                    Username
                  </th>
                  <th>
                    Portfolio Title
                  </th>
                  <th>
                    Portfolio detials
                  </th>
                  <th>
                    Portfolio Image
                  </th>
                  <th>
                    Created At
                  </th>
                  <th>
                    Options
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
