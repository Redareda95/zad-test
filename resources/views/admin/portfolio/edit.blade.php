@extends('admin.master')
@section('content')

<form method="POST" action="/admin/edit-portfolio/{{ $portfolio->id }}"  enctype="multipart/form-data">
                @csrf
<input type="hidden" name="_method" value="PATCH">
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Edit Portfolio</h4>
          <p class="card-description">
            Added By {{ $portfolio->user->name }} <code>On {{ $portfolio->created_at->toFormattedDateString() }}</code>
          </p>
      <div class="row">
        <div class="box box-danger">
      <div class="col-md-12">
        <div class="form-group">
          <label>Change Image</label>
          <input type="file" name="image" class="form-control"  id="imgInp">
            <h5>Old Photo</h5>
          <img id="blah" src="/uploads/{{ $portfolio->image }}" alt="your image" width="300" />
        </div>
      </div>
    </div>
    </div>
    </div>
<div class="box-footer">
  <button type="submit" class="btn btn-success btn-block btn-flat">Save Changes</button>
</div>
</div>
</div>
</div>
</form>
@if(count($errors))
<div class="alert alert-danger">
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
@endif
@endsection