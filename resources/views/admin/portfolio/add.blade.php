@extends('admin.master')
@section('content')

<form method="POST" action="/admin/add-portfolio-to-user/{{ $user->id }}"  enctype="multipart/form-data">
                @csrf
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Add Portfolio</h4>
          <p class="card-description">
            Add Portofolio to <code>{{ $user->name }}</code>
          </p>
      <div class="row">
        <div class="box box-danger">
          <div class="col-md-12">
            <div class="form-group">
              <label>Upload Image</label>
              <input type="file" name="image" class="form-control"  id="imgInp">
              <img id="blah" src="/addimage.gif" alt="your image" width="300" />
            </div>
          </div>
        </div>
      </div>
    </div>
<div class="box-footer">
  <button type="submit" class="btn btn-success btn-block btn-flat">Save Portfolio</button>
</div>
</div>
</div>
</div>
</form>
@if(count($errors))
<div class="alert alert-danger">
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
@endif
@endsection