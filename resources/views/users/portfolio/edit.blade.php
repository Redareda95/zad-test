@extends('website.master')
@section('content')

<!-- Start of contact us section
============================================= -->
<section id="contact" class="contact-us-section">
    <div class="container">
        <div class="row section-content">
            <div class="contact-us-contact">
                
                <div class="contact-area-form pt90">
                    <div class="row">
                        

                        <div class="col-sm-12">
                            <div class="contact-adress-form">
                                <form id="contact_form" action="/my-portfolio/edit-portfolio/{{ $portfolio->id }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                <input type="hidden" name="_method" value="PATCH">

                                    <div class="contact-info-item">
                                        <div class="form-group col-md-12">
                                            <h2><i>Change Image</i></h2>
                                            <label class="form@file__label"><input onchange="handleChange(event)" name="image" type="file" class="form@file" id="image-upload" multiple><div class="form@file__label-inner">
                                              <div class="svg" style="width: 150px !important;"></div>
                                              </div></label>
                                               <div class="form-group" id="oldPhotos">
                                                <h3 style="text-align: center;">
                                                    Old Image
                                                </h3>
                                                  <div class="row margin-bottom"> 
                                                     <div class="col-sm-3">
                                                        <img class="img-responsive" src="/uploads/{{ $portfolio->image }}" alt="Photo" style="width: 250px;height: 250px;">
                                                     </div>
                                                  </div>
                                                </div>
                                              <div class="error"></div>
                                              <div class="results"></div>
                                              <span>{{ $errors->first('image') }}</span>
                                        </div>    
                                    </div>
                                    
                                    <button class="text-uppercase" type="submit" value="save">Save<i class="fa fa-save" aria-hidden="true"></i></button> 

                                </form>
                            </div>
                        </div>
                        <!-- //.col-sm-9 -->
                    </div>
                </div>
                <!-- //.contact-area-form -->
            </div>
            <!-- //.contact-us-contact -->
        </div><!--  /.row-->
    </div><!--  /.container -->
</section>
<!-- End of contact us section
============================================= -->

@endsection