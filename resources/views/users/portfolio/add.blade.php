@extends('website.master')
@section('content')

<!-- Start of contact us section
============================================= -->
<section id="contact" class="contact-us-section">
    <div class="container">
        <div class="row section-content">
            <div class="contact-us-contact">
                
                <div class="contact-area-form pt90">
                    <div class="row">
                        

                        <div class="col-sm-12">
                            <div class="contact-adress-form">
                                <form id="contact_form" action="/add-portfolio" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="contact-info-item">
                                        <div class="form-group col-md-12">
                                            <h2><i>Add Image</i></h2>
                                            <label class="form@file__label"><input onchange="handleChange(event)" name="image" type="file" class="form@file" id="image-upload" multiple><div class="form@file__label-inner">
                                              <div class="svg" style="width: 150px !important;"></div>
                                              </div></label>
                                              <div class="error"></div>
                                              <div class="results"></div>
                                              <span>{{ $errors->first('image') }}</span>
                                        </div>    
                                    </div>
                                    
                                    <button class="text-uppercase" type="submit" value="save">Save<i class="fa fa-save" aria-hidden="true"></i></button> 

                                </form>
                            </div>
                        </div>
                        <!-- //.col-sm-9 -->
                    </div>
                </div>
                <!-- //.contact-area-form -->
            </div>
            <!-- //.contact-us-contact -->
        </div><!--  /.row-->
    </div><!--  /.container -->
</section>
<!-- End of contact us section
============================================= -->

@endsection