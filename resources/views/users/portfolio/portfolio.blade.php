@extends('website.master')
@section('content')

<div class="container bootstrap snippet padding-portfolio">
    <div class="row">
        <div class="col-sm-12">
        <h3>My <code> - Portfolio</code></h3>
      </div>
    </div>
    <hr>
    <div class="row profile-row">
        <div class="col-sm-3"><!--left col-->

          <div class="text-center">
            
            <img src="/uploads/{{ auth()->user()->details->logo }}" class="avatar img-circle img-thumbnail" alt="{{ auth()->user()->name }}">
          </div>
          </hr>
          <br>
          <div class="panel panel-default">
            <div class="panel-heading h6">
                User Information <i class="fa fa-link fa-1x"></i>
            </div>
            <div class="panel-body">
                <p class="word-break-p">
                    <?= strip_tags(auth()->user()->details->desc) ?>    
                </p>
            </div>
            
          </div>
          <hr>
          <div class="panel panel-default">
            <div class="panel-heading h6">
                Extra Information <i class="fa fa-link fa-1x"></i>
            </div>
            
            <div class="panel-body">
                <p class="word-break-p">
                    <?= strip_tags(auth()->user()->details->extra_details) ?>    
                </p>
            </div>
          </div>
          
        </div><!--/col-3-->
        <div class="col-sm-9 white-div-profile">
        <br/>
       
        @forelse($portfolio as $pro)
         <div class="item item-grid">
            <div class="item-wrap">
              <div class="row">
                        <div class="col-md-6">
                          <a href="/my-portfolio/edit-portfolio/{{ $pro->id }}" class="btn btn-app" style="background-color: #5ac695; color:white;"> 
                              <i class="fa fa-edit"></i> Edit
                          </a>
                        </div>
                        <div class="col-md-6">
                          <form action="/delete-portfolio/{{ $pro->id }}" method="POST" id="deletePost" style="display: inline-block;">
                              @csrf
                              <a onclick="if (!confirm('Are You Sure You Want to Delete')) { return false }" href="javascript:document.getElementById('deletePost').submit();" class="btn btn-app delete" style="background-color: #ec2711;color: white;">
                                <i class="fa fa-trash-o"></i> Delete
                              </a>
                          </form>
                        </div>
                      </div>
                <div class="work-item">
                    <div class="work-pic">
                        <img src="/uploads/{{ $pro->image }}">
                    </div>
                    <!-- //img -->
                    <div class="hover-content">
                        <div class="hover-text text-center">
                            <a href="/uploads/{{ $pro->image }}" data-lightbox="roadtrip">
                                <span class="ti-fullscreen"></span>
                            </a>
                        </div>
                        <!-- //light-box-img -->
                    </div>

                </div>
            </div>
        </div>
        @empty
        {{ 'No Portofolio Yet' }}
         @endforelse
        </div><!--/col-9-->
    </div><!--/row-->
  </div>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endsection