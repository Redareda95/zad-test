@extends('website.master')
@section('content')

<section class="container bootstrap snippet padding-portfolio">
    <div class="container">
        <div class="contact_title">
            <h2>{{ \Auth::user()->name }} Profile</h2>
        </div>
        <form class="form" action="/my-profile" method="POST" id="registrationForm" enctype="multipart/form-data">

        @csrf

        <input type="hidden" name="_method" value="PATCH">

        <div class="container bootstrap snippet">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
              <div class="col-sm-3"><!--left col-->
                <div class="text-center">
                  <img src="/uploads/{{ \Auth::user()->details->logo }}" class="avatar img-circle img-thumbnail" alt="{{ \Auth::user()->name }}">
                  <h6>Profile Picture</h6>
                  <input type="file" class="text-center center-block file-upload" name="logo">
                </div>
              </hr>
              <br>
                  
              <nav class="list-group">
                 <a class="list-group-item active" href="#">
                 	<i class="icon-head"></i> My Profile 
                 </a>
                
                 <a class="list-group-item with-badge" href="/my-portfolio/all">
                 	<i class="icon-tag"></i> Portfolio Images 
                  <span class="badge badge-primary badge-pill">
                    {{ \App\Portfolio::where('user_id', \Auth::user()->id)->count() }}
                  </span>
                 </a>
              </nav>
          
                  
                </div><!--/col-3-->
                <div class="col-sm-9">
                    <ul class="nav nav-tabs">
                      <li class="active">
                        <a data-toggle="tab" href="#home">
                          Account Credintials &amp; Information
                        </a>
                      </li>
                    </ul>

                      
                  <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <hr>
                              <div class="form-group">
                                  <div class="col-xs-6">
                                      <h4>Full Name *</h4>

                                      <input type="text" class="form-control" name="name" id="name" value="{{ \Auth::user()->name }}" required>
                                  </div>
                              </div>
                              <div class="form-group"> 
                                  <div class="col-xs-6">
                                    <h4>E-Mail Address *</h4>
                                      <input type="email" class="form-control" name="email" id="email" value="{{\Auth::user()->email }}" required>
                                  </div>
                              </div>
                              <h6 class="title text-center" style="padding-bottom:15px;">
                                If You need to change password
                                <small> if not needed leave it empty</small>
                              </h6>
                              <div class="form-group">
                                  <div class="col-xs-6">
                                      <h4>Password</h4>
                                      <input type="password" class="form-control" name="password" id="password" placeholder="Minimum 6 Characters" autocomplete="off">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="col-xs-6">
                                    <h4>Password Confirmation</h4>
                                      <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                                  </div>
                              </div>             
                      <hr>
                      
                      <div class="form-group">
                        <div class="col-xs-6">
                          <h4>Profile Description *</h4>
                          <textarea name="desc" class="form-control" placeholder="Write down In Brief a Description about you" rows="4"><?= strip_tags(\Auth::user()->details->desc); ?></textarea>
                        </div>
                      </div>                          
          
                      <div class="form-group">
                          <div class="col-xs-6">
                            <h4>Extra Details <code>Add Any Extra Details about you, Social Links, Mobile Number ,Etc..</code></h4>
                            <textarea name="extra_details" class="form-control" placeholder="Add Any Extra Details about you, Social Links, Mobile Number ,Etc.." rows="5"><?= strip_tags(\Auth::user()->details->extra_details); ?></textarea>
                          </div>
                        </div> 
                        <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-success" type="submit">
                                  <i class="fa fa-save"></i>
                                  Save
                                </button>
                            </div>
                        </div>              
                     </div><!--/tab-pane-->
                      </div>
                      </div><!--/tab-pane-->
                  </div><!--/tab-content-->

                </div><!--/col-9-->
            </div><!--/row-->
          </form>
</section>
@endsection