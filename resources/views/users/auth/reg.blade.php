<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Zwaar :: Register</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../../vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="../../vendors/css/vendor.bundle.addons.css">
  <link rel="stylesheet" type="text/css" href="/css/register.css">

  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../../css/style.css">
  <!-- endinject -->
<link rel="shortcut icon" href="\favicon.png">
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper auth p-0 theme-two">
        <div class="row d-flex align-items-stretch">
          <div class="col-md-4 banner-section d-none d-md-flex align-items-stretch justify-content-center">
            <div class="slide-content bg-2">
            </div>
          </div>
          <div class="col-12 col-md-8 bg-white">
            <div class="auto-form-wrapper d-flex align-items-center justify-content-center flex-column">
              <div class="nav-get-started">
                <p>Already have an account?</p>
                <a class="btn get-started-btn" href="/login">SIGN IN</a>
              </div>
              <form action="/register" method="POST" enctype="multipart/form-data">
                @csrf
                <h3 class="mr-auto">Register</h3>
                <p class="mb-5 mr-auto">Enter your details below.</p>
                <div class="row">
                  <div class="form-group col-md-6">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="mdi mdi-account-outline"></i></span>
                      </div>
                      <input type="text" class="form-control" placeholder="Full Name" name="name">
                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                       @endif
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="mdi mdi-email"></i></span>
                      </div>
                      <input type="email" class="form-control" placeholder="Email Address" name="email">
                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                       @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="mdi mdi-lock-outline"></i></span>
                      </div>
                      <input type="password" class="form-control" placeholder="Password" name="password">
                      
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="mdi mdi-lock-outline"></i></span>
                      </div>
                      <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                      
                    </div>
                  </div>
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                   @endif
                   @if ($errors->has('password_confirmation'))
                  <span class="help-block col-md-12">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
                  @endif
                  <br>
                  </div>

                  <div>
                    <div class="col-lg-6 form-group">
                        <h4 class="mr-auto">Upload Profile Image</h4>
                        <label class=newbtn>
                            <img id="blah" src="/profile.png" >
                            <input type="file" name="logo" class="form-control"  id="imgInp">
                        </label>
                        @if ($errors->has('logo'))
                          <span class="help-block col-md-12">
                              <strong>{{ $errors->first('logo') }}</strong>
                          </span>
                        @endif
                      </div>
                    <div class="col-lg-12 form-group" style="padding-bottom: 15px">
                        <textarea class="form-control" name="desc" cols="92" rows="3" placeholder="Write down In Brief a Description about you"></textarea>
                        @if ($errors->has('desc'))
                          <span class="help-block col-md-12">
                              <strong>{{ $errors->first('desc') }}</strong>
                          </span>
                        @endif
                    </div>
                    <div class="col-lg-12 form-group" style="padding-bottom: 15px">
                        <textarea class="form-control" name="extra_details" cols="92" rows="3" placeholder="Add Any Extra Details about you, Social Links, Mobile Number ,Etc.."></textarea>
                    </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary submit-btn">SIGN IN</button>
                </div>
                <div class="wrapper mt-5 text-gray">
                  <ul class="auth-footer text-gray">
                    <li><a href="/login">Have Account Already</a></li>
                  </ul>

                  <br>

                  <p class="footer-text">Copyright © {{ date('Y') }} MyAhmedReda. All rights reserved.</p>
                  
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="../../vendors/js/vendor.bundle.base.js"></script>
  <script src="../../vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="../../js/template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
   <script src="/js/register.js"></script>

  <!-- End custom js for this page-->
</body>

</html>