@extends('website.master')
@section('content')

<div class="container bootstrap snippet padding-portfolio">
    <div class="row">
        <div class="col-sm-12">
        <h3>{{ $query->name }} <code> - Profile</code></h3>
      </div>
    </div>
    <hr>
    <div class="row profile-row">
        <div class="col-sm-3"><!--left col-->

          <div class="text-center">
            
            <img src="/uploads/{{ $query->details->logo }}" class="avatar img-circle img-thumbnail" alt="{{ $query->name }}">
          </div>
          </hr>
          <br>
          <div class="panel panel-default">
            <div class="panel-heading h6">
                User Information <i class="fa fa-link fa-1x"></i>
            </div>
            <div class="panel-body">
                <p class="word-break-p">
                    <?= strip_tags($query->details->desc) ?>    
                </p>
            </div>
            
          </div>
          <hr>
          <div class="panel panel-default">
            <div class="panel-heading h6">
                Extra Information <i class="fa fa-link fa-1x"></i>
            </div>
            
            <div class="panel-body">
                <p class="word-break-p">
                    <?= strip_tags($query->details->extra_details) ?>    
                </p>
            </div>
          </div>
          
        </div><!--/col-3-->
        <div class="col-sm-9 white-div-profile">
        <br/>
       
        @forelse($portfolio as $pro)
         <div class="item item-grid">
            <div class="item-wrap">
                <div class="work-item">
                    <div class="work-pic">
                        <img src="/uploads/{{ $pro->image }}">
                    </div>
                    <!-- //img -->
                    <div class="hover-content">
                        <div class="hover-text text-center">
                            <a href="/uploads/{{ $pro->image }}" data-lightbox="roadtrip">
                                <span class="ti-fullscreen"></span>
                            </a>
                        </div>
                        <!-- //light-box-img -->
                    </div>
                </div>
            </div>
        </div>
        @empty
        {{ 'No Portofolio Yet' }}
         @endforelse
        </div><!--/col-9-->
    </div><!--/row-->
  </div>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endsection