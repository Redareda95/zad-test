<!-- Start of footer section
============================================= -->
<footer>
    <div class="footer-area footer-2">
        <div class="container">
            <div class="row">
                <div class="copy-right-area text-center">
                    <span>© <a href="https://myahmedreda.wordpress.com/">MyAhmedReda</a> 2019 - All Rights Reserved </span>
                </div>
                <!-- //copy-right-area -->
            </div><!--  /.container -->
        </div><!--  /.row-->
    </div><!--  /.footer-area -->
</footer>
<!-- End of footer section
============================================= -->



<!--  Js Library -->
<script src="/assets/js/jquery-2.1.4.min.js"></script>
<!-- Include  for bootstrap -->
<script src="/assets/js/bootstrap.min.js"></script>
<!-- Include isotope Js -->
<script src="/assets/js/jquery.isotope.min.js"></script>
<!-- Include lightbox -->
<script src="/assets/js/lightbox.js"></script>
<!-- Include circle-effect.js -->
<script src="/assets/js/circle-effect.js"></script>
<!-- Include Video js -->
<script src="/assets/js/jquery.magnific-popup.min.js"></script>
<!-- Include Owl-carousel -->
<script src="/assets/js/owl.carousel.min.js"></script>
<!-- Include Counter up -->
<script src="/assets/js/jquery.counterup.min.js"></script>
<script src="/assets/js/waypoints.min.js"></script>
<script src="/js/add.js"></script>

<!-- Include script.js -->
<script src="/assets/js/script.js"></script>
<script type="text/javascript">
$(document).ready(function() {


var readURL = function(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('.avatar').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
  }
}


$(".file-upload").on('change', function(){
  readURL(this);
});
});
</script>
<script type="text/javascript">
    $('li > a').click(function() {
    $('li').removeClass();
    $(this).parent().addClass('active');
});
</script>
@if($flash = session('message'))
<script>
    alert('{{ $flash }}');
</script>
@endif
</body>
</html>