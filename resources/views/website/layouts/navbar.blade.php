<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?= $seo->title ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="Keywords" content="{{ $seo->keywords }}">
<meta name="description" content="<?= strip_tags($seo->desc); ?>">

<meta name="author" content="{{ $seo->author }}">


<link rel="shortcut icon" href="/favicon.png">

<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- css-include -->

<!-- boorstrap -->
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
<!-- themify-icon.css -->
<link rel="stylesheet" type="text/css" href="/assets/css/themify-icons.css">
<!-- owl-carousel -->
<link rel="stylesheet" type="text/css" href="/assets/css/owl.carousel.css">
<!-- light-box -->
<link rel="stylesheet" type="text/css" href="/assets/css/lightbox.css">
<!-- video css -->
<link rel="stylesheet" type="text/css" href="/assets/css/video.min.css">
<!-- menu.css -->
<link rel="stylesheet" type="text/css" href="/assets/css/menu.css">
<!-- style -->
<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
<!-- responsive.css -->
<link rel="stylesheet" type="text/css" href="/assets/css/responsive.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/profile.css">
</head>


<body>
<div id="preloader"></div>
<!-- Start of nav bar 
============================================= -->
<nav id="poppin-nav">
    <div id="nav-off"></div>
    <div id="header-logo-1" class="text-center">
      @if(\Auth::check())
        @if(Auth::user()->hasRole('admin'))
        <img src="/assets/img/logo/logo.png" alt="P.P">
        @else
        <img src="/uploads/{{ Auth::user()->details->logo }}" alt="P.P">
        @endif
      @else
        <img src="/assets/img/logo/logo.png" alt="P.P">
      @endif
    </div>
    <ul id="menu">
        <li class="has-submenu {{ Request::is('/') ? 'text-capitalize' : '' }}">
            <a href="/">home</a>
        </li>
       @if(\Auth::check())
       <li class="has-submenu {{ Request::segment(1) === 'add-portfolio' ? 'text-capitalize' : null }}">
            <a href="/add-portfolio">Add Images To Portfolio</a>
        </li>
        <li class="has-submenu {{ Request::segment(1) === 'my-profile' ? 'text-capitalize' : null }}">
            <a href="/my-profile">My Profile</a>
        </li>
        <li class="has-submenu {{ Request::segment(1) === 'my-portfolio' ? 'text-capitalize' : null }}">
            <a href="/my-portfolio/all">My Portfolio</a>
        </li>
        <li class="has-submenu">
            <form action="/logout" method="POST" style="display:inline!important; padding-left: 15px;">
                @csrf
                <a href="javascript:;" onclick="parentNode.submit();" style="text-decoration: none;">
                    <i class="fa fa-lock"></i> Logout 
                </a>
            </form>
        </li>
        @else
        <li class="has-submenu}">
            <a href="/login">Login</a>
        </li>
        <li class="has-submenu">
            <a href="/register">Create Account</a>
        </li>
        @endif

    </ul>
</nav>
<div id="menu-overlay"></div>
<!-- End of nav bar 
============================================= -->



<!-- Start of header
============================================= -->
<header id="site-header" class="not-stuck">
    <div class="container">
        <div class="row">
            <div id="header-logo">
                <a href="/">
                    @if(\Auth::check())
                    @if(Auth::user()->hasRole('admin'))
                    <img src="/assets/img/logo/logo.png" alt="P.P" class="img-logo">
                    @else
                    <img src="/uploads/{{ Auth::user()->details->logo }}" alt="P.P" class="img-logo">
                    @endif
                  @else
                    <img src="/assets/img/logo/logo.png" alt="P.P" class="img-logo">
                  @endif
                </a>
            </div>

            <div id="menu-burger" class="pull-right not-stuck">
                <div id="menu-bar">
                    <span class="icon-bar top"></span>
                    <span class="icon-bar middle"></span>
                    <span class="icon-bar bottom"></span>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End of header
============================================= -->
