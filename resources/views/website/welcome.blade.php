@extends('website.master')
@section('content')

<!-- Start of page head section
============================================= -->
<section id="page-head" class="page-head-section">
    <div class="page-head-overlay"></div>
    <div id="page-head-effect" class="page-head-effect">
        <canvas id="demo-canvas"></canvas>
    </div>
    <!-- // page-head-style -->
    <div class="container">
        <div class="row">
            <div class="banner-content">
                <div class="page-head-content">
                    <div class="page-head-title text-capitalize pb40">
                        <h1>{{ 'Zwaar Task' }}</h1>
                        <span class="descrip">Creative Portfolios System</span>
                    </div>
                    <!-- //title -->
                </div>
                <!-- /.page-head-content -->
            </div>
        </div><!--  /.row-->
    </div><!--  /.container -->
</section>
<!-- End of page head section
============================================= -->

@if(!$users->isEmpty())
<!-- Start of portfolio section
============================================= -->
<section id="portfolio" class="portfolio-section">
    <div class="container">
        <div class="row">
            <div class="portfolio-content">
                <div class="portfolio-tab pb55">

                    <div class="portfolio-tab-text-pic row">
                        <div id="posts">
                            @foreach($users as $user)
                            <div class="item item-grid">
                                <div class="item-wrap">
                                    <div class="work-item">
                                        <div class="work-pic">
                                            <img src="/uploads/{{ $user->details->logo }}">
                                        </div>
                                        <!-- //img -->
                                        <div class="hover-content">
                                            <div class="hover-text text-center">
                                                <a href="/uploads/{{ $user->details->logo }}" data-lightbox="roadtrip">
                                                    <span class="ti-fullscreen"></span>
                                                </a>
                                            </div>
                                            <!-- //light-box-img -->
                                            <div class="project-description text-uppercase ul-li">
                                                <h3>
                                                    <a href="/user/{{ $user->id }}">
                                                        {{ $user->name }}
                                                    </a>
                                                </h3>
                                                <ul class="project-catagorry text-capitalize">
                                                    <li>
                                                        <a>
                                                            <?= substr(strip_tags($user->details->desc), 0, 150) . "..."; ?> 
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- //project-description -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /item -->
                            @endforeach
                        </div><!--//posts-->
                    </div><!--//portfolio-tab-text-pic-->
                </div><!--//portfolio-tab-->
            </div><!--//portfolio-content -->
        </div><!--  /.row -->
    </div><!--  /.conatiner -->
</section>
<!-- End of portfolio section
============================================= -->
@endif

@endsection